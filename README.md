#compile-R

Documentation for the installation of [R](http://cran.r-project.org) from source
in the computation servers at AGPF INRA.

Describes the procedure for the configuration and installation of R from source
in a linux computer.
Includes details on pre-requisites, configuration options and post-installation
steps.





<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
